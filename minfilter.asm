# Author: Marcin Puc
# Description: a minimum image filter
# Usage: program prompts for mask size, input file path and output file path

.data
	maskPrompt:	.asciiz	"Enter mask size: "
	inputPrompt:	.asciiz	"Enter input file: "
	outputPrompt:	.asciiz	"Enter output file: "
	errWrongMask:	.asciiz	"Incorrect mask size\n"
	errOpenFail:	.asciiz "Could not open file\n"
	errWrongHeader:	.asciiz	"Incorrect header\n"
	errWrongBPP:	.asciiz	"Not a 24bit bitmap\n"
	inputName:	.space	256
	outputName:	.space	256
	bmpheader:	.align	2			# BMP header - always 14 bytes
			.space	14
	dibheader:	.align	2			# DIB header - max 124 bytes
			.space	124
	rowbuffer:	.space	2400			# buffer for a single image row


.text
# main
	li $v0, 4
	la $a0, maskPrompt
	syscall			# prompt for mask size

	li $v0, 5
	syscall			# read mask size from stdin
	move $s2, $v0		# s2 = mask size

	andi $at, $s2, 1
	bnez $at, promptFile	# check if mask size is odd

	li $v0, 4
	la $a0, errWrongMask
	syscall			# if mask is even - error, exit program

	j exit

	promptFile:
	#set row counter
	addi $v1, $s2, -1
	
	li $v0, 4
	la $a0, inputPrompt
	syscall			# prompt for input filename

	li $v0, 8
	la $a0, inputName
	li $a1, 256
	syscall			# read filename from stdin
	
	#promptFileOUT:
	li $v0, 4
	la $a0, outputPrompt
	syscall			# prompt for output filename
	
	li $v0, 8
	la $a0, outputName
	li $a1, 256
	syscall			# read filename from stdin

	# remove trailing newline
	move $t0, $a0

	newline_loop:
	lb $t1, ($t0)

	beq $t1, '\n', overwrite
	beqz $t1, remove2

	addi $t0, $t0, 1
	j newline_loop

	overwrite:
	sb $zero, ($t0)
	
	remove2:
	la $t0, inputName
	
	newline_loop2:
	lb $t1, ($t0)

	beq $t1, '\n', overwrite2
	beqz $t1, openfile

	addi $t0, $t0, 1
	j newline_loop2

	overwrite2:
	sb $zero, ($t0)

	openfile:
	li $v0, 13
	la $a0, inputName
	li $a1, 0
	syscall			# open file for reading

	bgez $v0, saveDescr

	li $v0, 4
	la $a0, errOpenFail
	syscall			# file could not be opened

	j exit

	saveDescr:
	move $s0, $v0		# s0 = infile descriptor
	
	openFile_OUT:
	li $v0, 13
	la $a0, outputName
	li $a1, 1
	syscall			# open OUTPUT file for writing
	
	bgez $v0, saveDescrOUT
	
	li $v0, 4
	la $a0, errOpenFail
	syscall			# file could not be opened
	
	j closeINfile
	
	saveDescrOUT:
	move $s1, $v0		# s1 = outfile descriptor

	li $v0, 14
	move $a0, $s0
	la $a1, bmpheader
	li $a2, 14
	syscall			# read bmp file header from file
	
	li $v0, 15
	move $a0, $s1
	#la $a1, bmpheader
	#li $a2, 14
	syscall			# write bmp header back to new file

	li $t0, 0x4D42
	lhu $t1, ($a1)
	beq $t1, $t0, loadDIB	# check if file is a BMP

	#errorWH
	li $v0, 4
	la $a0, errWrongHeader
	syscall

	j closefile

	loadDIB:
	li $v0, 14
	move $a0, $s0
	#la $a1, bmpheader
	li $a2, 4
	syscall			# load 1st DIB field -> number of bytes in DIB
	
	li $v0, 15
	move $a0, $s1
	#la $a1, bmpheader
	#li $a2, 4
	syscall			# write back 1st DIB field

	lw $t0, ($a1)
	addi $t0, $t0, -4	# calculate remaining DIB bytes to read from file

	li $v0, 14
	move $a0, $s0
	la $a1, dibheader
	move $a2, $t0
	syscall			# read the rest of DIB
	
	li $v0, 15
	move $a0, $s1
	#la $a1, dibheader
	#move $a2, $t0
	syscall			# write back rest of DIB

	lhu $t0, dibheader+10	# BPP field
	beq $t0, 24, initFilter	# check if BPP == 24

	#errorNot24bit
	li $v0, 4
	la $a0, errWrongBPP
	syscall

	j closefile

	initFilter:
	lw $s3, dibheader	# s3 = width [B]
	lw $s4, dibheader+4	# s4 = height

	mul $s5, $s3, 24
	addiu $s5, $s5, 31
	srl $s5, $s5, 3
	andi $s5, 0xFFFFFFFC	# s5 = row size (WIKIPEDIA FORMULA)
	
	mul $s3, $s3, 3
	
	#initStack
	addi $t0, $s2, -1
	srl $t1, $s2, 1		# t0 = [mask size] - 1; t1 = [mask size] / 2
	
	#loadRow:
	mul $s6, $s2, $s5
	sub $sp, $sp, $s6	# sp -= [mask size] * [row size]
	move $s6, $sp
	
	loadRow:
	li $v0, 14
	move $a0, $s0
	move $a1, $s6
	move $a2, $s5
	syscall			# load one row on stack
	
	bgt $t0, $t1, writeRowToFile
	
	beqz $t0, initPointers
	
	addi $t0, $t0, -1
	add $s6, $s6, $s5
	j loadRow
	
	writeRowToFile:
	li $v0, 15
	move $a0, $s1
	move $a1, $s6
	move $a2, $s5
	syscall			# write one row to output file
	
	addi $t0, $t0, -1
	add $s6, $s6, $s5
	j loadRow
	
	initPointers:
	move $t0, $sp		# t0 = first row (sp)
	
	addi $t8, $s2, -1
	mul $t8, $t8, $s5
	add $t8, $t8, $s3
	add $t8, $t8, -3
	add $t8, $t8, $sp	# t8 = end offset
	
	srl $t1, $s2, 1
	mul $t1, $t1, $s5
	add $t1, $t1, $sp	# t1 = middle row ( sp + ([mask size] / 2) * [row size] )
	
	move $t2, $t1		# t2 = processed row
	
	srl $t3, $s2, 1
	mul $t3, $t3, 3
	add $t3, $t3, $t1	#set t3 to pixel start-processing number
	
	la $t4, rowbuffer
	add $t9, $t4, $s3	# t9 = effective buffer end
	
	preloadBorder:
	#t2 -> stack memory
	#t4 -> buffer
	lbu $t5, ($t2)
	sb $t5, ($t4)
	lbu $t5, 1($t2)
	sb $t5, 1($t4)
	lbu $t5, 2($t2)
	sb $t5, 2($t4)
	
	addi $t2, $t2, 3
	addi $t4, $t4, 3
	
	blt $t2, $t3, preloadBorder
	
	#PRELOADED!!!
	
	srl $t2, $s2, 1
	mul $t2, $t2, 3
	sub $t2, $s3, $t2
	add $t2, $t2, $t1
	
	move $t3, $t4		# t3 = buffer pointer
	
	move $t4, $sp		# t4 = first-pixel-in-mask pointer (INIT)
	
	beginMask:
	addi $t6, $s2, -1
	mul $s6, $t6, $s5
	mul $s7, $t6, 3
	
	mul $t6, $t6, 3
	add $t6, $t6, $t4	# t6 = last-pixel-in-current-row pointer
	
	#mul $s6, $s6, $s5
	add $s6, $s6, $s7
	add $s6, $s6, $t4	# s6 = last-pixel-in-mask pointer
	
	li $a0, 255		# BMIN
	li $a1, 255		# GMIN
	li $a2, 255		# RMIN
	
	nextRow:
	move $t5, $t4		# t5 -> current processed 
	
# T7 OCCUPIED
	checkB:
	lbu $t7, ($t5)
	bge $t7, $a0, checkG
	move $a0, $t7
	checkG:
	lbu $t7, 1($t5)
	bge $t7, $a1, checkR
	move $a1, $t7
	checkR:
	lbu $t7, 2($t5)
	bge $t7, $a2, nextInRow
	move $a2, $t7
# T7 VACANT
	
	nextInRow:
	addi $t5, $t5, 3
	ble $t5, $t6, checkB
	
	bgt $t5, $s6, nextMask
	
	add $t4, $t4, $s5
	add $t6, $t6, $s5
	
	j nextRow
	
	nextMask:
	#addiu $t2, $t2, 3
	
	sb $a0, ($t3)
	sb $a1, 1($t3)
	sb $a2, 2($t3)
	
	addiu $t3, $t3, 3
	
	beq $s6, $t8, copyRestOfRowToBuffer
	
	addi $t4, $t4, 3
	addi $v0, $s2, -1
	mul $v0, $v0, $s5
	sub $t4, $t4, $v0
	
	j beginMask
	
	copyRestOfRowToBuffer:
	lbu $s7, ($t2)
	sb $s7, ($t3)
	lbu $s7, 1($t2)
	sb $s7, 1($t3)
	lbu $s7, 2($t2)
	sb $s7, 2($t3)
	
	addiu $t2, $t2, 3
	addiu $t3, $t3, 3
	
	blt $t3, $t9, copyRestOfRowToBuffer
	
	#load whole processed row from buffer to  output file
	li $v0, 15
	move $a0, $s1
	la $a1, rowbuffer
	move $a2, $s5
	syscall
	
	addi $v1, $v1, 1
	beq $v1, $s4, postProcessing
	
	#read next row from file into memory
	readNewRow:
	li $v0, 14
	move $a0, $s0
	move $a1, $t0
	move $a2, $s5
	syscall
	
	sub $v0, $t8, $s3
	addi $v0, $v0, 3
	
	add $t0, $t0, $s5
	ble $t0, $v0, checkMidPointer
	move $t0, $sp
	
	checkMidPointer:
	add $t1, $t1, $s5
	ble $t1, $v0, resetPointers
	move $t1, $sp
	
	resetPointers:
	move $t2, $t1
	la $t4, rowbuffer
	
	srl $t3, $s2, 1
	mul $t3, $t3, 3
	add $t3, $t3, $t1
	
	j preloadBorder
	
	postProcessing:
	sub $t5, $t8, $s3
	addi $t5, $t5, 3
	
	srl $t4, $s2, 1
	move $a0, $s1
	move $a2, $s5
	
	saveToFile:
	add $t1, $t1, $s5
	ble $t1, $t5, writeLine
	move $t1, $sp
	
	writeLine:
	li $v0, 15
	move $a1, $t1
	syscall
	
	#decrementCounter:
	addi $t4, $t4, -1
	bgtz $t4, saveToFile
	
	#doCleanup:
	mul $t0, $s2, $s5
	add $sp, $sp, $t0
	
	closefile:
	li $v0, 16
	move $a0, $s1
	syscall		# close output file
	
	closeINfile:
	li $v0, 16
	move $a0, $s0
	syscall		# close input file
	
	exit:
	li $v0, 10
	syscall		# exit
